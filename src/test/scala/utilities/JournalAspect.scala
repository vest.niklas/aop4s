package example

import aop.{Aspect, Joinpoint, Pointcut}
import Joinpoint.*

import scala.collection.mutable.Buffer

/**
 * A simple test aspect that injects a log statement per matched pointcut expression.
 * @param journal A logging buffer.
 */
class JournalAspect(val journal: Buffer[String] = Buffer.empty[String]) extends Aspect:

  private def log(tag: String): Unit = journal += tag

  override protected def before(pcs: Pointcut*)(f: Invocation => Seq[AnyRef]) =
    super.before(pcs: _*) {
        case i@Invocation(target, method, args) =>
            log(s"before ${method.getName}")
            f(i)
    }

  override protected def after(pcs: Pointcut*)(f: Returning => Unit) =
    super.after(pcs: _*) {
        case r@Returning(target, method, result) =>
            f(r)
            log(s"after ${method.getName}")
    }

  override protected def around(pcs: Pointcut*)(f: (Around, Seq[AnyRef]) => AnyRef) =
    // doesn't play nicely with logIfMatched sadly
    super.around(pcs: _*) { 
      case (a@Around(_, method, _), args) =>
        log(s"[around] before ${method.getName}")
        val res = f(a, args)
        log(s"[around] after ${method.getName}")
        res
    }