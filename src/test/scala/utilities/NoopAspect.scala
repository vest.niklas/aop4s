package utilities

import aop.*
import Joinpoint.*

/**
 * An aspect that allows subclasses to specify only a pointcut in the `before` DSL call. The arguments
 * supplied by the `Invocation` [[Joinpoint]] are simply returned without further action. This is a 
 * useful trait when combined with [[JournalAspect]]. Matches are merely logged but the method dispatch
 * isn't modified along the way.
 * 
 * @see example.WildcardRegexAspect
 */
trait NoopBefore extends Aspect:
  def beforeN(pc: Pointcut) = 
    super.before(pc)(_.args)

/**
 * An aspect that allows subclasses to specify only a pointcut in the `after` DSL call. The `Returning` 
 * [[Joinpoint]] is completely ignored. This is a useful trait when combined with [[JournalAspect]]. 
 * Matches are merely logged but the method dispatch isn't modified along the way.
 * 
 * @see example.WildcardRegexAspect
 */
trait NoopAfter extends Aspect:
  def afterN(pc: Pointcut) = 
    super.after(pc)(_ => ())

/**
 * An aspect that allows subclasses to specify only a pointcut in the `around` DSL call. The result of
 * proceeding with the information presented by the `Around` [[Joinpoint]] is simply returned without further 
 * action. This is a useful trait when combined with [[JournalAspect]]. Matches are merely logged but the 
 * method dispatch isn't modified along the way.
 * 
 * @see example.WildcardRegexAspect
 */
trait NoopAround extends Aspect:
  def aroundN(pcs: Pointcut) = 
    super.around(pcs) { case (Around(_, _, proceed), args) => proceed(args) }

trait NoopAspect 
    extends NoopBefore 
    with NoopAfter 
    with NoopAround
  