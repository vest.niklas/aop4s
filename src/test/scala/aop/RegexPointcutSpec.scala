package aop

import aop.*
import aop.syntax.*

import org.scalatest.*
import flatspec.*
import matchers.*

import scala.collection.mutable.{Buffer, Stack}

import example.*

class RegexPointcutSpec extends Spec:

  "A pointcut expression \".*\"" should "match all invocations" in {

    // before
    // around before
    // around after
    // after
    val aspect = new WildcardRegexAspect

    val repo = AspectRegistry()
        .consider(aspect)
        .summonProxyFor[UsersRepository, MockUsersRepository]

    repo.findById("")
    
    aspect.journal.length shouldBe 4

    repo.findAll
    aspect.journal.length shouldBe 8  
    
  }

  "A pointcut expression \"find.*\"" should "match all invocations starting with 'find'" in {
    
    // before
    val aspect = new WildcardAtEndRegexAspect

    val repo = AspectRegistry()
        .consider(aspect)
        .summonProxyFor[UsersRepository, MockUsersRepository]

    repo.findById("")
    aspect.journal.length shouldBe 1

    repo.findAll
    aspect.journal.length shouldBe 2

  }

  "A pointcut expression \".*ById\"" should "match all invocations ending with 'ById'" in {
    
    // before
    val aspect = new WildcardAtBeginningRegexAspect

    val repo = AspectRegistry()
        .consider(aspect)
        .summonProxyFor[UsersRepository, MockUsersRepository]

    repo.findById("")
    aspect.journal.length shouldBe 1

    repo.findAll
    aspect.journal.length shouldBe 1

  }