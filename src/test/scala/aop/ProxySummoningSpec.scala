package aop

import aop.syntax.*
import example.*

class ProxySummoningSpec extends Spec:

    "The aspect registry" should "be able to summon a proxy for a class with default constructor" in {
        given AspectRegistry = AspectRegistry()
        noException should be thrownBy weaved[UsersRepository, MockUsersRepository]
    }

    it should "throw an exception when no default constructor is available" in {
        given AspectRegistry = AspectRegistry()
        an [UnsupportedOperationException] should be thrownBy weaved[UsersRepository, BadUsersRepository]
    }