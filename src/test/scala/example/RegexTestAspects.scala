package example

import aop.*
import Joinpoint.*
import utilities.*

class WildcardRegexAspect extends JournalAspect with NoopAspect:

  beforeN(`*`)

  aroundN(`*`)

  afterN(`*`)

class WildcardAtEndRegexAspect extends JournalAspect with NoopAspect:
  beforeN(pointcut("find.*"))

class WildcardAtBeginningRegexAspect extends JournalAspect with NoopAspect:
  beforeN(pointcut(".*ById"))

class WildcardAtBegginningAndEndRegexAspect extends JournalAspect with NoopAspect:
  beforeN(pointcut(".*By.*"))

class WildcardInTheMiddleRegexAspect extends JournalAspect with NoopAspect:
  beforeN(pointcut("find.*Id"))