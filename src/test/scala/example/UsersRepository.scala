package example

case class User(name: String, age: Int)

trait UsersRepository:
  def findById(id: String): Option[User]
  def findAll: Iterator[User]

// an implementation with a default constructor, this should work perfectly
class MockUsersRepository extends UsersRepository:
  private val users = List(User("Nik", 22), User("NotNik", 38))
  override def findById(id: String) = users.find(_.name == id)
  override def findAll = users.iterator

// traits don't have a default constructor, those can't be instantiated
trait BadUsersRepository extends UsersRepository:
  private val users = List(User("Nik", 22), User("NotNik", 38))
  override def findById(id: String) = users.find(_.name == id)
  override def findAll = users.iterator