package aop

import scala.collection.mutable.Buffer
import Joinpoint.*

trait Aspect:

  val beforeAdvices = Buffer.empty[Advice.Before]
  val aroundAdvices = Buffer.empty[Advice.Around]
  val afterAdvices = Buffer.empty[Advice.After]

  protected def pointcut(pc: String) = Pointcut(pc)

  protected def `*` = pointcut(".*")

  protected def before(pcs: Pointcut*)(f: Invocation => Seq[AnyRef]) =
    beforeAdvices += Advice.Before(pcs, f)

  protected def around(pcs: Pointcut*)(f: (Around, Seq[AnyRef]) => AnyRef) =
    aroundAdvices += Advice.Around(pcs, f)

  protected def after(pcs: Pointcut*)(f: Returning => Unit) =
    afterAdvices += Advice.After(pcs, f)
