package aop.syntax

import aop.AspectRegistry

import scala.reflect.ClassTag

def weaved[A : ClassTag, B <: A : ClassTag](using ar: AspectRegistry) =
  ar.summonProxyFor[A, B]