package aop

import java.lang.reflect.Method

enum Joinpoint:

  case Invocation(target: Object, method: Method, args: Seq[AnyRef])

  case Around(target: Object, method: Method, proceed: Seq[AnyRef] => AnyRef)

  case Returning(target: Object, method: Method, invocationResult: AnyRef)
