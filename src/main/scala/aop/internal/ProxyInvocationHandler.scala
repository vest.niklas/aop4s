package aop.internal

import aop.{AspectRegistry, Joinpoint}

import java.lang.reflect.{InvocationHandler, Method}
import scala.reflect.{classTag, ClassTag}

class ProxyInvocationHandler[A: ClassTag](aspectRegistry: AspectRegistry) extends InvocationHandler:

  private val target: A =
    val clazz = classTag[A].runtimeClass
    val defaultConstructor = clazz.getConstructors.find(_.getParameterCount == 0)

    defaultConstructor match
      case Some(c) => c.newInstance().asInstanceOf[A]
      case None => throw new UnsupportedOperationException(
        s"No default constructor available for class ${clazz.getName}")

  private def targetObject =
    target.asInstanceOf[AnyRef]

  override def invoke(o: Any, method: Method, args: Array[AnyRef]): Any =
    // args is null if the method has 0 parameters
    val nonNullArgs = if args != null then args else Array.empty[AnyRef]
    val newArgs = weaveBeforeAdvices(method, nonNullArgs)
    val res = weaveAroundAdvices(method, newArgs)
    weaverAfterAdvices(method, res)
    res

  private def weaveAroundAdvices(method: Method, newArgs: Array[AnyRef]) =
    val firstMatchingAroundAdvice =
      aspectRegistry
        .aspects
        .flatMap(_.aroundAdvices)
        .find(_.pointcuts.exists(pc => method.getName.matches(pc.value)))

    val res =
      firstMatchingAroundAdvice
        .map { _.invoke(Joinpoint.Around(targetObject, method, method.invoke(target, _: _*)), newArgs) }
        .getOrElse(method.invoke(target, newArgs: _*))
    res

  private def weaverAfterAdvices(method: Method, res: AnyRef) =
    aspectRegistry.aspects.flatMap(_.afterAdvices).foreach { aa =>
      if aa.pointcuts.exists(pc => method.getName.matches(pc.value)) then
        aa.invoke(Joinpoint.Returning(targetObject, method, res))
    }

  private def weaveBeforeAdvices(method: Method, args: Array[AnyRef]) =
    aspectRegistry.aspects.flatMap(_.beforeAdvices).foldLeft(args) {
      case (arguments, beforeAdvice) =>
        if beforeAdvice.pointcuts.exists(pc => method.getName.matches(pc.value)) then
          beforeAdvice.invoke(Joinpoint.Invocation(targetObject, method, args)).toArray
        else 
          arguments
    }
