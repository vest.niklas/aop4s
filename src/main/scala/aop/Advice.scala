package aop

enum Advice:

  case Before(pointcuts: Seq[Pointcut], invoke: Joinpoint.Invocation => Seq[AnyRef])

  case Around(pointcuts: Seq[Pointcut], invoke: (Joinpoint.Around, Seq[AnyRef]) => AnyRef)

  case After(pointcuts: Seq[Pointcut], invoke: Joinpoint.Returning => Unit)

