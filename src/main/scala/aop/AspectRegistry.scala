package aop

import scala.collection.mutable.Buffer
import scala.reflect.{classTag, ClassTag}
import internal.ProxyInvocationHandler

class AspectRegistry:

  val aspects = Buffer.empty[Aspect]

  def consider(aspect: Aspect) =
    aspects += aspect
    this

  def summonProxyFor[Interf : ClassTag, Impl <: Interf : ClassTag]: Interf =
    java.lang.reflect.Proxy.newProxyInstance(
      getClass.getClassLoader,
      // summon proxy staisfying the interface
      Array(classTag[Interf].runtimeClass),
      // but then delegate to instances of `Impl` 
      ProxyInvocationHandler[Impl](this)
    ).asInstanceOf[Interf]
