aop4s
---
> A basic framework for aspect oriented programming scala

## Disclaimer

The library is not published yet, but it's in the working!

## Usage

Define the logic to weave aspects into!

```scala
case class User(name: String, age: Int)

trait UsersRepository:
  def findById(id: String): Option[User]
  def findAll: Iterator[User]

class MockUsersRepository extends UsersRepository:

  private val users = 
    List(User("me", 22), User("not_me", 38))

  override def findById(id: String) = 
    users.find(_.name == id)

  override def findAll = 
    users.iterator
```

Define aspects!

```scala
import aop.*
import Joinpoint.*

class LogBeforeFind extends Aspect:
  before(pointcut("find.*")) { case Invocation(target, method, args) =>
    val className = target.getClass.getSimpleName
    val methodName = method.getName
    val argsStrimg = args.mkString("Arguments(", ",", ")")
    println(s"$className#$methodName is called with $argsString")
    args // pass args on, without changing them. We're just logging here
  }
```

In your program, instantiate the aspects and pass them to an aspect registry!

```scala
import aop.*

// ...

given AspectRegistry = 
  AspectRegistry()
    .consider(LogBeforeFind())
    // .consider(TransactionAroundTx)
    // .consider(StoreInCacheAfter)
    // ...
```

Instead of creating the `MockUsersRepository` instance manually, let the library weave all aspects from the aspect registry into your implementation!

```scala
import aop.syntax.*

// use the MockUsersRepository implementation, 
// but assume the UsersRepository interface!
val repo = weaved[UsersRepository, MockUsersRepository]

// pass the high-tech repo on like any other dependency
val muhService = new Service(repo)

// or call the methods directly to see the magic happen
repo.findById("me")
```

If you are not a fan of implicit arguments, you can just call `summonProxyFor[I, Impl]` on the aspect registry directly.

```scala
val registry = AspectRegistry().consider(LogBeforeFind())
val repo = registry.summonProxyFor[UsersRepository, MockUsersRepository]
repo.findById("me")
```